#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "../include/compt_frequence.h"




int main(int argc, char* argv[]) {

//    printf("%s\n", argv[0]);    // program name
//    printf("%s\n", argv[1]);    // first argument
//    printf("%s\n", argv[2]);    // second argument

    char input_file[] = "src/blake.txt";
    char output_file[] = "src/output.txt";
    char stopWords[] = "src/stopWords.txt";

//    char* input_file = argv[1];
//    char* output_file = argv[2];
//    char* stopWords = argv[3];


    // Cr�ation d'une instance de FrequencyCounter
    FrequencyCounter counter;

    // Initialisation du compteur
    init_frequency_counter(&counter);

    // Lecture du fichier d'entr�e et comptage de la fr�quence des mots
    read_and_count_words(&counter, input_file);

    // Filtrage des mots vides
    filter_stop_words(&counter, stopWords);

    // Tri des mots par fr�quence
    sort_words_by_frequency(&counter);

    // �criture des r�sultats dans le fichier de sortie
    write_results(&counter, output_file);

    // Lib�ration de la m�moire
    cleanup_frequency_counter(&counter);

    printf("Programme termin� avec succ�s.\n");

    while (1){
        mode_interactive();
    }

    return 0;
}