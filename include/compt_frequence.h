#ifndef FREQUENCY_COUNTER_H
#define FREQUENCY_COUNTER_H

#define MAX_STRING_LENGTH 100

// Structure pour représenter un mot avec sa fréquence
typedef struct {
    char *word;   // the word
    int frequency;  // the frequency of the word
} WordFrequency;

// Structure pour représenter le compteur de fréquence
typedef struct {
    WordFrequency *words;       // the array of words
    int count;                // the number of unique words
} FrequencyCounter;

// Supprime le premier caractère d'une chaîne de caractères
void deleteFirstChar(char *str);

// Initialisation du compteur de fréquence
void init_frequency_counter(FrequencyCounter *counter);

// Libération de la mémoire allouée pour le compteur de fréquence
void cleanup_frequency_counter(FrequencyCounter *counter);

// Lecture du fichier et comptage des mots
void read_and_count_words(FrequencyCounter *counter, const char *filename);

// Tri des mots par fréquence
void sort_words_by_frequency(FrequencyCounter *counter);

// Écriture des résultats dans un fichier
void write_results(const FrequencyCounter *counter, const char *filename);

// function that filters the stop words
void filter_stop_words(FrequencyCounter *counter, const char *filename);

int count_words_in_file(const char *filename);
void concatenate_strings(const char *strings[], int num_strings, char *result);
void read_and_count_ngrams(FrequencyCounter *counter, const char *filename, int ngram_size);

int compare_word_frequency(const void *a, const void *b);

void mode_interactive();
#endif /* FREQUENCY_COUNTER_H */
