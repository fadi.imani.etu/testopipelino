#include "gtest/gtest.h"

// this is how we include our c code in the test
extern "C" {
    #include "../include/compt_frequence.h"
}
TEST(ExampleTests, test_init_frequency_counter) {
    FrequencyCounter counter;
    init_frequency_counter(&counter);
    EXPECT_EQ(0, counter.count);
    EXPECT_TRUE(counter.words==NULL);
}


TEST(ExampleTests, test_deleteFirstChar) {
    char str[] = "hello";
    deleteFirstChar(str);
    EXPECT_STREQ("ello", str);
}


TEST(ExampleTests, test_read_and_count_words) {
    FrequencyCounter counter;
    init_frequency_counter(&counter);
    char cwd[1024];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        printf("Current working directory: %s\n", cwd);
    }
    read_and_count_words(&counter, "test.txt");
    EXPECT_EQ(3, counter.count);
    EXPECT_STREQ("hello", counter.words[0].word);
    EXPECT_EQ(3, counter.words[0].frequency);
    EXPECT_STREQ("world", counter.words[1].word);
    EXPECT_EQ(1, counter.words[1].frequency);
    EXPECT_STREQ("foo", counter.words[2].word);
    EXPECT_EQ(2, counter.words[2].frequency);
    cleanup_frequency_counter(&counter);
}

TEST(ExampleTests, test_read_and_count_words_case_insensitive) {
    FrequencyCounter counter;
    init_frequency_counter(&counter);
    char cwd[1024];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        printf("Current working directory: %s\n", cwd);
    }
    read_and_count_words(&counter, "test.txt");
    EXPECT_EQ(3, counter.count);
    EXPECT_STREQ("hello", counter.words[0].word);
    EXPECT_EQ(3, counter.words[0].frequency);
    EXPECT_STREQ("world", counter.words[1].word);
    EXPECT_EQ(1, counter.words[1].frequency);
    EXPECT_STREQ("foo", counter.words[2].word);
    EXPECT_EQ(2, counter.words[2].frequency);
    cleanup_frequency_counter(&counter);
}

TEST(ExampleTests, test_read_and_count_words_ponctuation) {
    FrequencyCounter counter;
    init_frequency_counter(&counter);
    char cwd[1024];
    if (getcwd(cwd, sizeof(cwd)) != NULL) {
        printf("Current working directory: %s\n", cwd);
    }
    read_and_count_words(&counter, "test.txt");
    EXPECT_EQ(3, counter.count);
    EXPECT_STREQ("hello", counter.words[0].word);
    EXPECT_EQ(3, counter.words[0].frequency);
    EXPECT_STREQ("world", counter.words[1].word);
    EXPECT_EQ(1, counter.words[1].frequency);
    EXPECT_STREQ("foo", counter.words[2].word);
    EXPECT_EQ(2, counter.words[2].frequency);
    cleanup_frequency_counter(&counter);
}

TEST(ExampleTests, test_sort_words_by_frequency) {
    FrequencyCounter counter;
    init_frequency_counter(&counter);
    read_and_count_words(&counter, "test.txt");
    sort_words_by_frequency(&counter);
    EXPECT_EQ(3, counter.count);
    EXPECT_STREQ("hello", counter.words[0].word);
    EXPECT_STREQ("foo", counter.words[1].word);
    EXPECT_STREQ("world", counter.words[2].word);
    cleanup_frequency_counter(&counter);
}

TEST(ExampleTests, test_write_results) {
    FrequencyCounter counter;
    init_frequency_counter(&counter);
    read_and_count_words(&counter, "test.txt");
    sort_words_by_frequency(&counter);
    write_results(&counter, "output.txt");
    cleanup_frequency_counter(&counter);
    // check the output file
    FILE *f = fopen("output.txt", "r");
    char line[100];
    fgets(line, 100, f);
    EXPECT_STREQ("hello 3\n", line);
    fgets(line, 100, f);
    EXPECT_STREQ("foo 2\n", line);
    fgets(line, 100, f);
    EXPECT_STREQ("world 1\n", line);
    fclose(f);
}

TEST(ExampleTests, test_filter_stop_words) {
    FrequencyCounter counter;
    init_frequency_counter(&counter);
    read_and_count_words(&counter, "test.txt");
    sort_words_by_frequency(&counter);
    filter_stop_words(&counter, "stopWords.txt");
    write_results(&counter, "output.txt");
    EXPECT_EQ(3, counter.count);
    EXPECT_STREQ("hello", counter.words[0].word);
    EXPECT_EQ(3, counter.words[0].frequency);
    EXPECT_STREQ("foo", counter.words[1].word);
    EXPECT_EQ(2, counter.words[1].frequency);
    cleanup_frequency_counter(&counter);
}

TEST(ExampleTests, test_count_words_in_file) {
    EXPECT_EQ(6, count_words_in_file("test.txt"));
}

TEST(ExampleTests, test_concatenate_strings) {
    const char *arr[] = {"This", "is", "an", "array", "of", "strings"};
    int num_strings = sizeof(arr) / sizeof(arr[0]);
    char result[MAX_STRING_LENGTH] = "";

    concatenate_strings(arr, num_strings, result);
    EXPECT_STREQ("This is an array of strings ", result);
}

TEST(ExampleTests, test_compare_word_frequency) {
    WordFrequency a = {"hello", 3};
    WordFrequency b = {"world", 1};
    EXPECT_TRUE(compare_word_frequency(&a, &b) < 0);
}

TEST(ExampleTests, test_compare_word_frequency_when_equal) {
    WordFrequency a = {"hello", 3};
    WordFrequency b = {"world", 3};
    EXPECT_TRUE(compare_word_frequency(&a, &b) < 0);
}




//TEST(ExampleTests, test_read_and_count_ngrams) {
//    FrequencyCounter counter;
//    init_frequency_counter(&counter);
//    read_and_count_ngrams(&counter, "test.txt", 2);
//    EXPECT_EQ(2, counter.count);
//    EXPECT_STREQ("hello world", counter.words[0].word);
//    EXPECT_EQ(1, counter.words[0].frequency);
//    EXPECT_STREQ("world foo", counter.words[1].word);
//    EXPECT_EQ(1, counter.words[1].frequency);
//    cleanup_frequency_counter(&counter);
//}

TEST(ExampleTests, test_mode_interactive) {
    // this is just to make sure the function does not crash
    mode_interactive();
}






int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}